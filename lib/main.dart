import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const request_url =
    "https://api.hgbrasil.com/finance?format=json-cors&key=542c90a8";

void main() async {
  runApp(
    MaterialApp(
      title: "Conversor de Moedas",
      home: Home(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.white,
          hintColor: Colors.amber,
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
            focusedBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
            hintStyle: TextStyle(color: Colors.amber),
          )
      ),
    ) ,
  );
}

Future<Map> getData() async {
  http.Response response = await http.get(request_url);
  return json.decode(response.body);
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final realCtrl = TextEditingController();
  final dolarCtrl = TextEditingController();
  final euroCtrl = TextEditingController();

  double dolar;
  double euro;

  void _realChanged(String text){
    if(text.isEmpty){
      _clearAllInputs();
      return;
    }

    double real = double.parse(text);
    dolarCtrl.text = (real/dolar).toStringAsFixed(2);
    euroCtrl.text = (real/euro).toStringAsFixed(2);

  }

  void _dolarChanged(String text){
    if(text.isEmpty){
      _clearAllInputs();
      return;
    }

    double dolar = double.parse(text);
    realCtrl.text = (dolar * this.dolar).toStringAsFixed(2);
    euroCtrl.text = (dolar * this.dolar / euro).toStringAsFixed(2);
  }

  void _euroChanged(String text){
    if(text.isEmpty){
      _clearAllInputs();
      return;
    }

    double euro = double.parse(text);
    realCtrl.text = (euro * this.euro).toStringAsPrecision(2);
    dolarCtrl.text = (euro * this.euro / dolar).toStringAsPrecision(2);
  }

  void _clearAllInputs(){
    realCtrl.text = "";
    dolarCtrl.text = "";
    euroCtrl.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("\$ Conversor \$"),
        backgroundColor: Colors.amber,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _clearAllInputs,
          ),
        ],
      ),
      body: FutureBuilder<Map>(
        future: getData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                child: SpinKitRotatingCircle(
                  color: Colors.amber,
                  size: 50,
                )
              );
            default:
              if (snapshot.hasError) {
                return Center(
                  child: Text("Erro ao carregar dados!",
                      style: TextStyle(color: Colors.amber, fontSize: 25),
                      textAlign: TextAlign.center),
                );
              } else {
                dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

                return SingleChildScrollView(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Icon(Icons.monetization_on, size: 150, color: Colors.amber ,),
                      buildTextField("Reais", "R\$ ", realCtrl, _realChanged),
                      Divider(),
                      buildTextField("Dolar", "US\$ ", dolarCtrl, _dolarChanged),
                      Divider(),
                      buildTextField("Euros", "€ ", euroCtrl, _euroChanged)
                    ],
                  ),
                );
              }
          }
        },
      ),
    );
  }
}

Widget buildTextField(String label, String prefix, TextEditingController ctrl, Function changed){
  return TextField(
    decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.amber, fontSize: 20),
        border: OutlineInputBorder(),
        prefixText: prefix
    ),
    style: TextStyle(color: Colors.amber, fontSize: 25),
    keyboardType: TextInputType.numberWithOptions(decimal: true), //compatível em iOS
    onChanged: changed,
    controller: ctrl,
  );
}
